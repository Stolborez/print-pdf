<?php


class FileAct extends File
{
    public function create($inFile)
    {
        /* ARRAY */
        $array_date = array('1' => 'января', '2' => 'февраля', '3' => 'марта', '4' => 'апреля', '5' => 'мая', '6' => 'июня', '7' => 'июля', '8' => 'августа', '9' => 'сентября', '10' => 'октября', '11' => 'ноября', '12' => 'декабря');
        $date = $this->date->format('j') . ' ' . $array_date[$this->date->format('n')] . ' ' . $this->date->format('Y');
        $date_curr = date('j') . ' ' . $array_date[date('n')] . ' ' . date('Y');

        $pdf = new PDF_REP();
        $pdf->AddPage();

        // Add a Unicode font (uses UTF-8)
        $pdf->AddFont('Arial', '', '', true);
        $pdf->AddFont('Arial', 'bd', '', true);
        $pdf->SetFont('Arial', '', 8);
        $pdf->ln();
        $pdf->SetFont('Arial', 'bd', 14);
        $pdf->Cell(30, 4, 'Акт №' . $this->num . ' от ' . $this->date->format('j') . '.' . $this->date->format('m') . '.' . $this->date->format('Y') . 'г.', 0, 'L');
        $pdf->ln(6);
        $pdf->Line($pdf->GetX(), $pdf->GetY(), $pdf->GetX() + 190, $pdf->GetY());
        $pdf->ln(10);
        $pdf->SetFont('Arial', '', 9);
        $pdf->Cell(30, 4, 'Поставщик:', 0, 'L');
        $pdf->SetFont('Arial', 'bd', 9);
        $pdf->MultiCell(160, 4, 'ИП "Моновицкий Дмитрий Сергеевич" БИК 047102651 ПАО Сбербанк
Корр. счет 30101810800000000651 РС 40802810267170006390 ИНН получателя 861900967402
Адрес:  628331 Тюменская обл. Нефтеюганский район г.п. Пойковский д48а кв 3', 0, 'L');
        $pdf->ln();
        $pdf->SetFont('Arial', '', 9);
        $pdf->Cell(30, 4, 'Покупатель:', 0, 'L');
        $pdf->SetFont('Arial', 'bd', 9);
        $pdf->MultiCell(160, 4, $this->temp_text, 0, 'L');
        $pdf->ln();
        $pdf->SetFont('Arial', '', 9);
        $pdf->Cell(30, 4, 'Основание:', 0, 'L');
        $pdf->SetFont('Arial', 'bd', 9);
        $pdf->MultiCell(160, 4, 'Счет на оплату № ' . date('j') . date('m') . date('y') . '-' . $this->num . ' от ' . $date_curr . 'г.', 0, 'L');
        $pdf->ln();
        $pdf->SetFont('Arial', '', 9);
        $pdf->Cell(10, 4, '№', 1, 'С');
        $pdf->Cell(70, 4, 'Товары (работы, услуги)', 'TBR', 'С');
        $pdf->Cell(20, 4, 'Кол-во', 'TBR', 'С');
        $pdf->Cell(20, 4, 'Ед.', 'TBR', 'С');
        $pdf->Cell(30, 4, 'Цена', 'TBR', 'С');
        $pdf->Cell(30, 4, 'Сумма', 'TBR', 'С');
        $pdf->ln();
        $pdf->SetFont('Arial', '', 8);
        $pdf->Row([['widths' => 10, 'text' => '1'],
            ['widths' => 70, 'text' => $this->name_tarif],
            ['widths' => 20, 'text' => '1'],
            ['widths' => 20, 'text' => 'шт'],
            ['widths' => 30, 'text' => $this->price . 'p.'],
            ['widths' => 30, 'text' => $this->price . 'p.']]);

        $pdf->ln();
        $pdf->SetX(130);
        $pdf->SetFont('Arial', 'bd', 9);
        $pdf->Cell(40, 4, 'Итого:', 0, 'R');
        $pdf->Cell(30, 4, $this->price . 'p.', 0, 'L');
        $pdf->ln();
        $pdf->SetX(130);
        $pdf->Cell(30, 4, 'Без налога (НДС):', 0, 'L');
        $pdf->ln();
        $pdf->SetFont('Arial', '', 8);
        $pdf->Cell(30, 4, 'Всего оказано услуг 1 , на сумму ' . substr($this->price . 'p.', 0, -5) . ' руб.', 0, 'L');

        $pdf->ln();
        $pdf->SetFont('Arial', 'bd', 9);
        $pdf->Cell(30, 4, 'Сумма прописью ' . $this->priceText, 0, 'L');

        $pdf->ln(6);
        $pdf->SetFont('Arial', '', 9);
        $pdf->Cell(30, 4, 'Вышеперечисленные услуги выполнены полностью и в срок. Заказчик претензий по объему, качеству', 0, 'L');
        $pdf->ln();
        $pdf->SetFont('Arial', '', 9);
        $pdf->Cell(30, 4, 'и срокам оказания услуг не имеет.', 0, 'L');
        $pdf->ln(6);
        $pdf->Line($pdf->GetX(), $pdf->GetY(), $pdf->GetX() + 190, $pdf->GetY());

        $pdf->ln(6);
        $pdf->SetFont('Arial', 'bd', 10);
        $pdf->Cell(30, 4, 'ИСПОЛНИТЕЛЬ', 0, 'L');
        $pdf->SetX(130);
        $pdf->Cell(30, 4, 'ЗАКАЗЧИК', 0, 'L');

        $pdf->ln();
        $pdf->SetFont('Arial', '', 8);
        $pdf->Cell(30, 4, 'Генеральный директор,', 0, 'L');

        $pdf->ln();
        $pdf->SetFont('Arial', '', 8);
        $pdf->Image('img.png', 12, 125, 40, 25);
        $pdf->SetXY(48, 134);
        $pdf->Cell(30, 4, 'Моновицкий Д.С.', 'B', $align = 'R');
        $pdf->SetXY(130, 134);
        $pdf->Cell(30, 4, '                                   ', 'B', $align = 'R');

        $filename = ' Акт выполненных работ № ' . $this->date->format('j') . $this->date->format('m') . $this->date->format('y') . '-' . $this->num . ' от ' . $date . '.pdf';

        if ($inFile) {
            $pdf->Output($filename);
            return $filename;
        } else {
            return $pdf->Output($filename, 'I');
        }
    }
}