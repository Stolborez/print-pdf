<?php

use PHPMailer\PHPMailer\PHPMailer;

require 'vendor/autoload.php';
require('File.php');
require('FileAct.php');
require('FileBill.php');
require('FileExelAct.php');

$filenameBill = '';
$filenameAct = '';
$filenameActExel = '';
$numDoc = getNum();

if (isset($_GET['open_bill'])) {
    $pdfBill = new FileBill($numDoc);
    $pdfBill->create(false);
} elseif (isset($_GET['open_act'])) {
    $pdfAct = new FileAct($numDoc);
    $pdfAct->create(false);
} elseif (isset($_GET['send_b'])) {
    $pdfBill = new FileBill($numDoc);
    $filenameBill = $pdfBill->create(true);
    $pdfAct = new FileAct($numDoc);
    $filenameAct = $pdfAct->create(true);
    $pdfExelAct = new FileExelAct($numDoc);
    $filenameActExel = $pdfExelAct->create(true);
}
if (!empty($_GET['send']) || !empty($_GET['mail'])) {
    //Отправка основного сообщения заказчику
    $mail = getPHPMailer($_GET['mail'], '', $filenameAct, $filenameBill);
    if (!$mail->send()) {
        echo 'Ошибка при отправке. Ошибка: ' . $mail->ErrorInfo;
    } else {
        echo 'Сообщение успешно отправлено';
    }
    //Отправка дублирующего сообщения
    $mail = getPHPMailer('metallportal24@mail.ru', $filenameActExel, $filenameAct, $filenameBill);
    if (!$mail->send()) {
        echo 'Ошибка при отправке. Ошибка: ' . $mail->ErrorInfo;
    }
    //Удаление временных файлов
    unlink($filenameBill);
    unlink($filenameAct);
    unlink($filenameActExel);
}

/**
 * @param $address
 * @param $filenameActExel
 * @param $filenameAct
 * @param $filenameBill
 * @return PHPMailer
 */
function getPHPMailer($address, $filenameActExel, $filenameAct, $filenameBill)
{
    $mail = new PHPMailer;
    $mail->CharSet = 'utf-8';
    $mail->isSMTP();
    $mail->Host = 'ssl://smtp.mail.ru';;
    $mail->SMTPAuth = true;
    $mail->Username = 'ВАША_ПОЧТА@mail.ru';    //Логин
    $mail->Password = 'ПАРОЛЬ_ОТ_ПОЧТЫ';        //Пароль
    $mail->SMTPSecure = 'ssl';
    $mail->Port = 465;
    $mail->setLanguage('ru');
    $mail->setFrom('metallportal24@mail.ru', 'metallportal24');//metallportal24@mail.ru
    $mail->addAddress($address);    //Получатель
    $mail->addAttachment($filenameAct);              //Прикрепить файл
    $mail->addAttachment($filenameActExel);              //Прикрепить файл
    $mail->addAttachment($filenameBill);              //Прикрепить файл
    $mail->AddCustomHeader('Precedence: bulk;');
    $mail->Subject = 'Счет на оплату metallportal24.ru';
    $mail->Body = ' ';
    return $mail;
}

function getNum()
{
    $f = fopen("counter.txt", 'a+');
    flock($f, 1);
    $counts = fread($f, filesize("counter.txt"));
    $num = $counts + 1;
    fputs($f, (string)$num);
    flock($f, 3);
    fclose($f);
    $f = fopen("counter.txt", 'w+');
    flock($f, 2);
    fputs($f, "");
    fputs($f, (string)$num);
    flock($f, 3);
    fclose($f);
    return $num;
}


?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>
<html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>
<html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Данные организации</title>
    <link rel="stylesheet" href="style.css">
    <link rel="icon" href="http://vladmaxi.net/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="http://vladmaxi.net/favicon.ico" type="image/x-icon">
    <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script><![endif]-->
</head>
<body>
<section class="container">
    <div class="login">
        <h1>Введите данные организации</h1>
        <form onsubmit="return checkForm(this)" action="">
            <p><input type="text" name="name" value="" placeholder="Наименование организации" autofocus required></p>
            <p><input type="text" name="inn" value="" placeholder="ИНН" autofocus required></p>
            <p><input type="text" name="kpp" value="" placeholder="КПП для ЮЛ"></p>
            <p><input type="text" name="address" value="" placeholder="Адрес" autofocus required></p>
            <p><input type="text" name="mail" value="" placeholder="Почта для получения счета (mail@mail.ru)" autofocus
                      required pattern="\S+@[a-z]+.[a-z]+"></p>
            <p><select name="tarif">
                    <option value="1">Оказание информационных услуг тариф "продвинутый" на месяц. 2900р</option>
                    <option value="2">Оказание информационных услуг тариф "продвинутый" на год со скидкой 30%. 26700р
                    </option>
                    <option value="3">Оказание информационных услуг тариф "Профессионал" на месяц. 9799р</option>
                </select></p>

            <p class="submit"><input type="submit" name="open_act" value="Посмотреть акт"></p>
            <p class="submit"><input type="submit" name="open_bill" value="Посмотреть счет"></p>
            <p class="submit"><input type="submit" name="send_b" value="Отправить на почту"></p>
        </form>
    </div>
</section>
</body>
</html>