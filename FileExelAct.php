<?php


class FileExelAct extends File
{

    public function create($inFile)
    {
        $array_date = array('1' => 'января', '2' => 'февраля', '3' => 'марта', '4' => 'апреля', '5' => 'мая', '6' => 'июня', '7' => 'июля', '8' => 'августа', '9' => 'сентября', '10' => 'октября', '11' => 'ноября', '12' => 'декабря');
        $date = $this->date->format('j') . ' ' . $array_date[$this->date->format('n')] . ' ' . $this->date->format('Y');
        $date_curr = date('j') . ' ' . $array_date[date('n')] . ' ' . date('Y');

        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $spreadsheet = $reader->load("example.xlsx");
        $spreadsheet->getActiveSheet()
            ->getCell('B3')
            ->setValue('Акт №' . $this->num . ' от ' . $this->date->format('j') . '.' . $this->date->format('m') . '.' . $this->date->format('Y') . 'г.');
        $spreadsheet->getActiveSheet()
            ->getCell('F7')
            ->setValue($this->temp_text);
        $spreadsheet->getActiveSheet()
            ->getCell('F9')
            ->setValue('Счет на оплату № ' . date('j') . date('m') . date('y') . '-' . $this->num . ' от ' . $date_curr . 'г.');
        $spreadsheet->getActiveSheet()
            ->getCell('D13')
            ->setValue($this->name_tarif);
        $spreadsheet->getActiveSheet()
            ->getCell('U13')
            ->setValue('1');
        $spreadsheet->getActiveSheet()
            ->getCell('X13')
            ->setValue('шт');
        $spreadsheet->getActiveSheet()
            ->getCell('Z13')
            ->setValue($this->price);
        $spreadsheet->getActiveSheet()
            ->getCell('AD13')
            ->setValue($this->price);
        $spreadsheet->getActiveSheet()
            ->getCell('AD15')
            ->setValue($this->price);
        $spreadsheet->getActiveSheet()
            ->getCell('B18')
            ->setValue('Всего оказано услуг 1 , на сумму ' . substr($this->price . 'p.', 0, -5) . ' руб.');
        $spreadsheet->getActiveSheet()
            ->getCell('B19')
            ->setValue('Сумма прописью ' . $this->priceText);
        $spreadsheet->getActiveSheet()
            ->getCell('B21')
            ->setValue('Вышеперечисленные услуги выполнены полностью и в срок. Заказчик претензий по объему, качеству и срокам оказания услуг не имеет.');

        $filename = ' Акт выполненных работ № ' . $this->date->format('j') . $this->date->format('m') . $this->date->format('y') . '-' . $this->num . ' от ' . $date . '.xlsx';
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $writer->save($filename);
        $spreadsheet->disconnectWorksheets();
        unset($spreadsheet);

        return $filename;
    }
}