<?php


abstract class File
{
    protected $price;
    protected $name_tarif;
    protected $client_name;
    protected $client_inn;
    protected $client_kpp;
    protected $client_address;
    protected $temp_text;
    protected $total;
    protected $num;
    protected $date;
    protected $priceText;

    /**
     * ActRep constructor.
     * @param $numDoc
     */
    public function __construct($numDoc)
    {
        $this->num = $numDoc;

        $this->client_name = $_GET['name'];
        $this->client_inn = $_GET['inn'];
        $this->client_kpp = $_GET['kpp'];
        $this->client_address = $_GET['address'];
        $this->temp_text = "$this->client_name ИНН/КПП $this->client_inn ";
        $this->temp_text .= empty($this->client_kpp) ? "" : "/" . $this->client_kpp;
        $this->temp_text .= " Адрес: $this->client_address";

        if ($_GET['tarif'] == '1') {
            $this->price = '2 900.00';
            $this->name_tarif = 'Оказание информационных услуг тариф "продвинутый" на месяц';
            $this->total = 'Всего наименований 1, на сумму Две тысячи девятьсот рублей 00 коп.';
            $this->priceText = 'две тысячи девятьсот рублей 00 копеек';
            $this->date = (new DateTime(date('Y-m-d')))->modify('+1 month');
        } elseif ($_GET['tarif'] == '2') {
            $this->price = '26 700.00';
            $this->name_tarif = 'Оказание информационных услуг тариф "продвинутый" на год со скидкой 30%';
            $this->total = 'Всего наименований 1, на сумму Двадцать шесть тысяч семьсот рублей 00 коп.';
            $this->priceText = 'двадцать шесть тысяч семьсот рублей 00 копеек';
            $this->date = (new DateTime(date('Y-m-d')))->modify('+1 year');
        } elseif ($_GET['tarif'] == '3') {
            $this->price = '9 799.00';
            $this->name_tarif = 'Оказание информационных услуг тариф "Профессионал" на месяц';
            $this->total = 'Всего наименований 1, на сумму Девять тысяч семьсот девяносто девять рублей 00 коп.';
            $this->priceText = 'девять тысяч семьсот девяносто девять рублей 00 копеек';
            $this->date = (new DateTime(date('Y-m-d')))->modify('+1 month');
        }

    }

    abstract function create($inFile);
}